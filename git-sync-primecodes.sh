id=primecodes
dir=/workspace/$id
sh /workspace/ssh-keys/$id/install.sh
for x in $@; do
  if [ -d "$dir/$x" ]; then
    cd "$dir/$x"
    git pull
  else
    mkdir -p $dir
    cd $dir
    git clone ssh://git@gitlab.com/$id/$x
  fi
done