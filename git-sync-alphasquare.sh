sh /workspace/ssh-keys/m8228308850/install.sh
echo sync alphasquare...
if [ -d "/workspace/stock/alphasquare" ]; then
  cd "/workspace/stock/alphasquare"
  git pull
else
  mkdir -p /workspace/stock
  cd /workspace/stock
  git clone ssh://git@gitlab.com/m8228308850/alphasquare
fi
for x in $@; do
  sh /workspace/env/git-sync-alphasquare-downloader.sh $x
done